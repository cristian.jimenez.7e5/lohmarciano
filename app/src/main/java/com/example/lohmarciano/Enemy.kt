package com.example.lohmarciano

import android.content.Context
import android.graphics.*

class Enemy(context: Context, val screenX: Int, screenY: Int, initialVelocity: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.knekro)
    val width = screenX / 6f
    val height = screenY / 9f
    var positionX = screenX / 2
    var speed = initialVelocity
    var positionEnemy = RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)

        positionEnemy.left = positionX.toFloat()
        positionEnemy.top = 75f
        positionEnemy.right = positionEnemy.left + width
        positionEnemy.bottom = positionEnemy.top + height

    }

    fun updateEnemy(){
        if(positionX > screenX-width){
            speed = -50
        } else if(positionX < 0){
            speed = 50
        }
        positionX += speed
        positionEnemy.left = positionX.toFloat()
        positionEnemy.right = positionEnemy.left + width
    }

    fun drawEnemy(canvas: Canvas, paint: Paint){
        canvas.drawBitmap(bitmap, positionX.toFloat(), 75f, paint)
    }

    fun drawEnemyHitbox(canvas: Canvas, paint: Paint){
        paint.color = Color.RED
        paint.style = Paint.Style.STROKE
        canvas.drawRect(positionEnemy, paint)
    }
}