package com.example.lohmarciano

import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.navigation.fragment.findNavController
import com.example.lohmarciano.databinding.FragmentMenuBinding

class MenuFragment: Fragment() {

    lateinit var binding: FragmentMenuBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMenuBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mediaPlayer: MediaPlayer? = MediaPlayer.create(context, R.raw.tercero_del_mundo)
        mediaPlayer?.start()
        mediaPlayer?.setVolume(0.35f, 0.35f)
        mediaPlayer?.setOnCompletionListener {
            mediaPlayer.start()
        }

        binding.playButton.setOnClickListener {
            mediaPlayer?.stop()
            findNavController().navigate(R.id.action_menuFragment_to_gameFragment)
        }

        binding.aboutTheGame.setOnClickListener {
            // Inflate the custom dialog layout
            val dialogView = LayoutInflater.from(requireContext()).inflate(R.layout.dialog_about_game, null)

            // Create the dialog
            val dialog = AlertDialog.Builder(requireContext())
                .setView(dialogView)
                .create()

            // Set the click listener for the "Close" button in the dialog
            dialogView.findViewById<Button>(R.id.closeButton).setOnClickListener {
                dialog.dismiss()
            }

            // Show the dialog
            dialog.show()
        }
    }
}