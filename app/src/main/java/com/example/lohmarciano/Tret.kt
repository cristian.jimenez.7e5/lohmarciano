package com.example.lohmarciano

import android.content.Context
import android.graphics.*

class Tret(val context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.peepoclown)
    val width = screenX / 12f
    val height = screenY / 17f
    var positionY = screenY - 550f - height
    var positionX = 0f
    var speed = -80
    val positionShot = RectF()

    val explodeBitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.peepo_hit)

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)

        positionShot.left = positionX
        positionShot.top = positionY
        positionShot.right = positionShot.left + width
        positionShot.bottom = positionShot.top + height
    }

    fun updateShot(){
        positionY += speed
        positionShot.top = positionY
        positionShot.bottom = positionShot.top + height
    }

    fun drawShot(canvas: Canvas, paint: Paint){
        canvas.drawBitmap(bitmap, positionX, positionY, paint)
    }

    fun drawShotHitbox(canvas: Canvas, paint: Paint){
        paint.color = Color.RED
        paint.style = Paint.Style.STROKE
        canvas.drawRect(positionShot, paint)
    }

    fun updateBorders(){
        positionShot.left = positionX
        positionShot.right = positionShot.left + width
    }

    fun setPeepoHit(){
        bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.peepo_hit)
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }

    fun resetDefaultImage(){
        this.bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.peepoclown)
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)
    }
}