package com.example.lohmarciano

import android.content.Context
import android.graphics.*

class Player(context: Context, val screenX: Int, val screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.spaceship)
    val width = screenX / 4f
    val height = screenY / 6f
    var positionX = screenX * 2 / 5
    var speed = 0
    var positionAlly = RectF()
    val hitboxCorrector = 60f

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)

        positionAlly.left = positionX.toFloat()+hitboxCorrector
        positionAlly.top = screenY.toFloat()-475f
        positionAlly.right = positionAlly.left + width - 2*hitboxCorrector
        positionAlly.bottom = positionAlly.top + height - hitboxCorrector
    }

    fun updatePlayer(){
        if(positionX > screenX-width){
            positionX = (screenX-width).toInt()
        } else if(positionX < 0){
            positionX = 0
        }
        positionX += speed
        positionAlly.left = positionX.toFloat()+hitboxCorrector
        positionAlly.right = positionAlly.left + width - 2*hitboxCorrector
    }

    fun drawPlayer(canvas: Canvas, paint: Paint){
        canvas.drawBitmap(bitmap, positionX.toFloat(), screenY.toFloat()-500f, paint)
    }

    fun drawPlayerHitbox(canvas: Canvas, paint: Paint){
        paint.color = Color.RED
        paint.style = Paint.Style.STROKE
        canvas.drawRect(positionAlly, paint)
    }
}
