package com.example.lohmarciano

import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.lohmarciano.databinding.FragmentGameOverBinding

class GameOverFragment : Fragment() {

    lateinit var binding: FragmentGameOverBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentGameOverBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mediaPlayer: MediaPlayer? = MediaPlayer.create(context, R.raw.astronomia)
        mediaPlayer?.start()
        mediaPlayer?.setVolume(0.35f, 0.35f)
        mediaPlayer?.setOnCompletionListener {
            mediaPlayer.start()
        }

        binding.menuButton.setOnClickListener {
            mediaPlayer?.pause()
            findNavController().navigate(R.id.action_gameOverFragment_to_menuFragment)
        }

        val score = arguments?.getInt("score").toString()
        binding.scoreText.text = "Score: $score"

    }
}