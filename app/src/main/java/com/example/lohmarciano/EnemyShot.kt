package com.example.lohmarciano

import android.content.Context
import android.graphics.*

class EnemyShot(context: Context, screenX: Int, screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.marta_clown)
    val width = screenX / 10f
    val height = screenY / 15f
    var positionX = 0f
    var positionY = 200f
    var speed = 70
    val positionEnemyShot = RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width.toInt(), height.toInt(),false)

        positionEnemyShot.left = positionX
        positionEnemyShot.top = positionY
        positionEnemyShot.right = positionEnemyShot.left + width
        positionEnemyShot.bottom = positionEnemyShot.top + height
    }

    fun updateEnemyShot(){
        positionY += speed
        positionEnemyShot.left = positionX
        positionEnemyShot.right = positionEnemyShot.left + width
        positionEnemyShot.top = positionY
        positionEnemyShot.bottom = positionEnemyShot.top + height
    }

    fun drawEnemyShot(canvas: Canvas, paint: Paint){
        canvas.drawBitmap(bitmap, positionX, positionY, paint)
    }

    fun drawEnemyShotHitbox(canvas: Canvas, paint: Paint){
        paint.color = Color.RED
        paint.style = Paint.Style.STROKE
        canvas.drawRect(positionEnemyShot, paint)
    }
}