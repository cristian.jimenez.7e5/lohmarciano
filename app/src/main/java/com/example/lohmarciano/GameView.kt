package com.example.lohmarciano

import android.content.Context
import android.graphics.*
import android.media.MediaPlayer
import android.media.SoundPool
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.fragment.app.findFragment
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class GameView(context: Context, private val size: Point): SurfaceView(context){

    var canvas: Canvas = Canvas()
    val paint: Paint = Paint()

    var playing = true
    var score = 0

    val player = Player(context, size.x, size.y)
    var firstEnemy = Enemy(context, size.x, size.y, (40..80).random())
    var secondEnemy = Enemy(context, size.x, size.y, -(40..80).random())

    val background = BitmapFactory.decodeResource(context.resources, R.drawable.fondo_lohmarciano)
    val shot = Tret(context, size.x, size.y)
    var shotAction = false

    var hasCollidedToEnemy = false
    var hasCollidedToAlly = false

    var lives = 3

    lateinit var firstEnemyShot: EnemyShot
    lateinit var secondEnemyShot: EnemyShot
    var enemyShotAction = false
    var enemyCooldown = 100

    var mediaPlayer: MediaPlayer? = MediaPlayer.create(context, R.raw.cat_jam)
    val soundPool = SoundPool.Builder().setMaxStreams(5).build()
    val mondongoSound = soundPool.load(context, R.raw.mondongo, 0)
    val venganceSound = soundPool.load(context, R.raw.im_vengance, 1)


    init {
        startGame()
        startMusic()
    }
    fun startGame(){
        CoroutineScope(Dispatchers.Main).launch{
            while(playing){
                draw()
                update()
                delay(100)
                enemyCooldown -= 100
                if(enemyCooldown == 0){
                    enemyShot()
                }
                if(lives == 0){
                    playing = false
                    mediaPlayer?.pause()
                    val action = GameFragmentDirections.actionGameFragmentToGameOverFragment(score)
                    findFragment<GameFragment>().findNavController().navigate(action)
                }
            }
        }
    }

    fun startMusic(){
        mediaPlayer?.start()
        mediaPlayer?.setVolume(0.35f, 0.35f)
        mediaPlayer?.setOnCompletionListener {
            mediaPlayer?.start()
        }
    }

    fun playSound(id: Int){
        val sound = soundPool.play(id, 10f, 10f, 0, 0, 1f)
        soundPool.setVolume(sound, 1f, 1f)
    }

    fun draw(){
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()

            canvas.drawBitmap(background, 0f, 0f, null)
            // Reset the paint style to fill
            paint.style = Paint.Style.FILL

            //SCORE
            paint.color = Color.YELLOW
            paint.textSize = 60f
            paint.textAlign = Paint.Align.RIGHT
            canvas.drawText("Score: $score", (size.x - paint.descent()), 75f, paint)

            //LIFES
            paint.color = Color.RED
            paint.textSize = 60f
            paint.textAlign = Paint.Align.LEFT
            canvas.drawText("  $lives ❤️", 0f, 75f, paint)

            //PLAYER
            player.drawPlayer(canvas, paint)
//            player.drawPlayerHitbox(canvas, paint)

            //ENEMIES
            firstEnemy.drawEnemy(canvas, paint)
//            firstEnemy.drawEnemyHitbox(canvas, paint)
            secondEnemy.drawEnemy(canvas, paint)
//            secondEnemy.drawEnemyHitbox(canvas, paint)

            //SHOT
            if(shotAction){
                shot.drawShot(canvas, paint)
//                shot.drawShotHitbox(canvas, paint)
            }

            //ENEMYSHOT
            if(enemyShotAction){
                firstEnemyShot.drawEnemyShot(canvas, paint)
//                firstEnemyShot.drawEnemyShotHitbox(canvas, paint)

                secondEnemyShot.drawEnemyShot(canvas, paint)
//                secondEnemyShot.drawEnemyShotHitbox(canvas, paint)
            }

            holder.unlockCanvasAndPost(canvas)
        }
    }

    fun update(){
        firstEnemy.updateEnemy()
        secondEnemy.updateEnemy()
        player.updatePlayer()
        if(shotAction){
            shot.updateShot()
            if(shot.positionY <= 0f){
                resetShot()
            }
            if (!hasCollidedToEnemy && (RectF.intersects(shot.positionShot, firstEnemy.positionEnemy) || RectF.intersects(shot.positionShot, secondEnemy.positionEnemy))) {
                shot.setPeepoHit()
                playSound(mondongoSound)
                score += 50
                hasCollidedToEnemy = true
                shot.speed = 0
                CoroutineScope(Dispatchers.Main).launch {
                    delay(1000)
                    resetShot()
                }
            }
        }

        if(enemyShotAction){
            firstEnemyShot.updateEnemyShot()
            secondEnemyShot.updateEnemyShot()

            if(firstEnemyShot.positionY >= size.y.toFloat()-200f || secondEnemyShot.positionY >= size.y.toFloat()-200f){
                enemyShotAction = false
                firstEnemyShot.positionY = 75f+firstEnemy.height
                secondEnemyShot.positionY = 75f+secondEnemy.height
                firstEnemyShot.updateEnemyShot()
                secondEnemyShot.updateEnemyShot()
            }


            if(!hasCollidedToAlly && (RectF.intersects(firstEnemyShot.positionEnemyShot, player.positionAlly) || RectF.intersects(secondEnemyShot.positionEnemyShot, player.positionAlly))){
                playSound(venganceSound)
                lives--
                hasCollidedToAlly = true
            }
            enemyCooldown = 2000
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            when(event.action){
                // Aquí capturem els events i el codi que volem executar per cadascún

                MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                    // Modifiquem la velocitat del jugador perquè es mogui?
                    if(event.x > size.x/2){
                        player.speed = 25
                    } else if(event.x < size.x/2){
                        player.speed = -25
                    }
                }

                MotionEvent.ACTION_UP -> {
                    player.speed = 0
                }
            }
        }
        return true
    }

    fun shot(){
        if(!shotAction){
            shotAction = true
            shot.resetDefaultImage()
            shot.positionX = player.positionX.toFloat()+player.width/3
            shot.updateBorders()
            shot.speed = -80
            hasCollidedToEnemy = false
        }
    }

    fun resetShot(){
        shotAction = false
        shot.positionY = size.y.toFloat()-550f
        shot.updateShot()
    }

    fun enemyShot(){
        enemyShotAction = true
        firstEnemyShot = EnemyShot(context, size.x, size.y)
        firstEnemyShot.positionX = firstEnemy.positionX.toFloat()+firstEnemy.width/3

        secondEnemyShot = EnemyShot(context, size.x, size.y)
        secondEnemyShot.positionX = secondEnemy.positionX.toFloat()+secondEnemy.width/3

        hasCollidedToAlly = false
    }
}
